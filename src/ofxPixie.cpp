#include "ofxPixie.h"

/* Function : FTDI_ClosePort
* Author	: ENTTEC
* Purpose  : Closes the Pixie Device Handle
* Parameters: none
**/
void ofxPixie::FTDI_ClosePort()
{
	if (device_handle != NULL)
		FT_Close(device_handle);
}

void ofxPixie::FTDI_Reload()
{
	WORD wVID = 0x0403;
	WORD wPID = 0x6001;
	FT_STATUS ftStatus;

	printf("\nReloading devices for use with drivers ");
	ftStatus = FT_Reload(wVID, wPID);
	// Must wait a while for devices to be re-enumerated
	Sleep(3500);
	if (ftStatus != FT_OK)
	{
		printf("\nReloading Driver FAILED");
	}
	else
		printf("\nReloading Driver D2XX PASSED");
}

/* Function : FTDI_SendData
* Author	: ENTTEC
* Purpose  : Send Data (DMX or other packets) to the USB Device
* Parameters: Label, Pointer to Data Structure, Length of Data
**/
int ofxPixie::FTDI_SendData(int label, uint8_t *data, int length)
{
	uint8_t end_code = DMX_END_CODE;
	FT_STATUS res = 0;
	DWORD bytes_to_write = length;
	DWORD bytes_written = 0;
	HANDLE event = NULL;
	int size = 0;
	// Form Packet Header
	uint8_t header[DMX_HEADER_LENGTH];
	header[0] = DMX_START_CODE;
	header[1] = label;
	header[2] = length & OFFSET;
	header[3] = length >> BYTE_LENGTH;
	// Write The Header
	res = FT_Write(device_handle, (uint8_t *)header, DMX_HEADER_LENGTH, &bytes_written);
	if (bytes_written != DMX_HEADER_LENGTH) return  NO_RESPONSE;
	// Write The Data
	res = FT_Write(device_handle, (uint8_t *)data, length, &bytes_written);
	if (bytes_written != length) return  NO_RESPONSE;
	// Write End Code
	res = FT_Write(device_handle, (uint8_t *)&end_code, ONE_BYTE, &bytes_written);
	if (bytes_written != ONE_BYTE) return  NO_RESPONSE;
	if (res == FT_OK)
		return TRUE;
	else
		return FALSE;
}

/* Function : FTDI_ReceiveData
* Author	: ENTTEC
* Purpose  : Receive Data (DMX or other packets) from the USB DEVICE
* Parameters: Label, Pointer to Data Structure, Length of Data
**/
int ofxPixie::FTDI_ReceiveData(int label, uint8_t *data, unsigned int expected_length)
{

	FT_STATUS res = 0;
	DWORD length = 0;
	DWORD bytes_to_read = 1;
	DWORD bytes_read = 0;
	uint8_t byte = 0;
	HANDLE event = NULL;
	char buffer[600];
	// Check for Start Code and matching Label
	while (byte != label)
	{
		while (byte != DMX_START_CODE)
		{
			res = FT_Read(device_handle, (uint8_t *)&byte, ONE_BYTE, &bytes_read);
			if (bytes_read == NO_RESPONSE) return  NO_RESPONSE;
		}
		res = FT_Read(device_handle, (uint8_t *)&byte, ONE_BYTE, &bytes_read);
		if (bytes_read == NO_RESPONSE) return  NO_RESPONSE;
	}
	// Read the rest of the Header Byte by Byte -- Get Length
	res = FT_Read(device_handle, (uint8_t *)&byte, ONE_BYTE, &bytes_read);
	if (bytes_read == NO_RESPONSE) return  NO_RESPONSE;
	length = byte;
	res = FT_Read(device_handle, (uint8_t *)&byte, ONE_BYTE, &bytes_read);
	if (res != FT_OK) return  NO_RESPONSE;
	length += ((uint32_t)byte) << BYTE_LENGTH;
	// Check Length is not greater than allowed
	if (length > DMX_PACKET_SIZE)
		return  NO_RESPONSE;
	// Read the actual Response Data
	res = FT_Read(device_handle, buffer, length, &bytes_read);
	if (bytes_read != length) return  NO_RESPONSE;
	// Check The End Code
	res = FT_Read(device_handle, (uint8_t *)&byte, ONE_BYTE, &bytes_read);
	if (bytes_read == NO_RESPONSE) return  NO_RESPONSE;
	if (byte != DMX_END_CODE) return  NO_RESPONSE;
	// Copy The Data read to the buffer passed
	memcpy(data, buffer, expected_length);
	return TRUE;
}


/* Function : FTDI_PurgeBuffer
* Author	: ENTTEC
* Purpose  : Clears the buffer used internally by the USB DEVICE
* Parameters: none
**/
void ofxPixie::FTDI_PurgeBuffer()
{
	FT_Purge(device_handle, FT_PURGE_TX);
	FT_Purge(device_handle, FT_PURGE_RX);
}


/* Function : isPixie
* Author	: ENTTEC
* Purpose  : Opens the USB DEVICE; Tests various parameters; outputs info
* Parameters: device num (returned by the List Device fuc)
**/
uint16_t ofxPixie::isPixie(int device_num)
{
	int ReadTimeout = 120;
	int WriteTimeout = 100;
	uint8_t Serial[4];
	uint8_t HWVersion = 0;
	long version;
	uint8_t major_ver, minor_ver, build_ver;
	int recvd = 0;
	uint8_t byte = 0;
	int size = 0;
	int res = 0;
	int tries = 0;
	uint8_t latencyTimer;
	FT_STATUS ftStatus;

	// Try at least 3 times 
	do {
		ftStatus = FT_Open(device_num, &device_handle);
		Sleep(500);
		tries++;
	} while ((ftStatus != FT_OK) && (tries < 3));

	if (ftStatus == FT_OK)
	{
		// D2XX Driver Version
		ftStatus = FT_GetDriverVersion(device_handle, (LPDWORD)&version);
		if (ftStatus == FT_OK)
		{
			major_ver = (uint8_t)version >> 16;
			minor_ver = (uint8_t)version >> 8;
			build_ver = (uint8_t)version & 0xFF;
			printf("\nD2XX Driver Version:: %02X.%02X.%02X ", major_ver, minor_ver, build_ver);
		}
		else
			printf("\nUnable to Get D2XX Driver Version");

		// Latency Timer -- used to minimize timeouts
		ftStatus = FT_GetLatencyTimer(device_handle, (PUCHAR)&latencyTimer);

		// These are important values that can be altered to suit your needs
		// Timeout in microseconds: Too high or too low value should not be used 
		FT_SetTimeouts(device_handle, ReadTimeout, WriteTimeout);
		// Buffer size in bytes (multiple of 4096) 
		FT_SetUSBParameters(device_handle, RX_BUFFER_SIZE, TX_BUFFER_SIZE);
		// Good idea to purge the buffer on initialize
		FT_Purge(device_handle, FT_PURGE_RX);

		// Check Version of Device to vaildate it's a Pixie
		res = NO_RESPONSE;
		tries = 0;
		do {
			res = FTDI_SendData(QUERY_HW_VERSION, (uint8_t *)&size, 0);
			Sleep(100);
			res = FTDI_ReceiveData(QUERY_HW_VERSION, &HWVersion, 1);
			tries++;
		} while ((res == NO_RESPONSE) && (tries < 3));

		if ((HWVersion & 0xF0) == PIXIE_HW_VERSION)
			printf("\n ---- PIXIE Found! Getting Device Information ------");
		else
		{
			printf("\n error: USB-Device is not Pixie: hw-version read: %d ", (HWVersion & 0xF0));
			return FALSE;
		}

		// Send Get Config to get Device Info
		printf("\nSending GET_CONFIG to PIXIE ... ");
		res = FTDI_SendData(GET_CONFIG_LABEL, (uint8_t *)&size, 0);
		if (res == NO_RESPONSE)
		{
			// try again - reset buffer on device 
			FT_Purge(device_handle, FT_PURGE_TX);
			res = FTDI_SendData(GET_CONFIG_LABEL, (uint8_t *)&size, 0);
			if (res == NO_RESPONSE)
			{
				FTDI_ClosePort();
				printf("\n USB Device did not reply");
				return  NO_RESPONSE;
			}
		}
		else
			printf("\n Pixie Connected Succesfully");
		// Receive Config Response
		printf("\nWaiting for GET_CONFIG REPLY packet... ");
		res = FTDI_ReceiveData(GET_CONFIG_LABEL, (uint8_t *)&PixieParams, sizeof(PixieConfigGet));
		if (res == NO_RESPONSE)
		{
			// try again - reset buffer on device 
			FT_Purge(device_handle, FT_PURGE_TX);
			res = FTDI_ReceiveData(GET_CONFIG_LABEL, (uint8_t *)&PixieParams, sizeof(PixieConfigGet));
			if (res == NO_RESPONSE)
			{
				FTDI_ClosePort();
				printf("\n USB Device did not reply");
				return  NO_RESPONSE;
			}
		}
		else
			printf("\n GET_CONFIG REPLY Received ... ");

		res = FTDI_SendData(GET_SN_LABEL, (uint8_t *)&size, 0);
		res = FTDI_ReceiveData(GET_SN_LABEL, (uint8_t *)&Serial, 4);
		// Display All Info available
		printf("\n\n-----------::PIXIE Connected [Information Follows]::------------");
		printf("\n\t  FIRMWARE VERSION: %d.%d", PixieParams.FirmwareMSB, PixieParams.FirmwareLSB);
		printf("\n\t  PERSONALITY: %d", PixieParams.Personality);
		printf("\n\t  LED Group Size: %d", PixieParams.GroupSize);
		printf("\n\t  RGB Order: %d", PixieParams.PixelOrder);
		printf("\n\t  LED STRIP TYPE: %d", PixieParams.LEDType);
		printf("\n\t  START Show On DMX LOSS: %d", PixieParams.PLayOnDMXLoss);
		printf("\n----------------------------------------------------------------\n\n");
		return TRUE;
	}
	else // Can't open Device 
		return FALSE;
}

void ofxPixie::SendRainbowFades()
{
	uint8_t rainbow1[] = { 0,143,255,0,75,130,0,0,255,255,0,0,255,255,0,127,255,0,0,255,0,127,255,0,255,0,0,0,75,130,0,143,255,0,143,255,0,75,130,0,0,255,255,0,0,255,255,0,127,255,0,0,255,0,127,255,0,255,0,0,0,75,130,0,143,255 };
	uint8_t rainbow2[] = { 0,143,255,0,143,255,0,143,255,0,75,130,0,75,130,0,75,130,0,0,255,0,0,255,0,0,255,255,0,0,255,0,0,255,0,0 };
	uint8_t rainbow3[] = { 255,255,0,255,255,0,255,255,0,127,255,0,127,255,0,127,255,0,0,255,0,0,255,0,0,255,0,255,255,0,255,255,0 };
	int counter = 0;
	int intensity = 200;
	int step_up = FALSE;
	int fade_count = 1;
	uint8_t dmxdata[MAX_PIXIE_CHANNELS + 1];
	int i = 0;

	if (device_handle != NULL)
	{
		// Looping to Send DMX data
		printf("\n Press Enter to Start Rainbow Fades (lasts 10 sec):");
		_getch();
		do {
			i = 0;
			if (intensity <= 10)
			{
				intensity = 10;
				step_up = TRUE;
			}
			if (intensity > 200)
			{
				intensity = 200;
				step_up = FALSE;
				fade_count += 1;
			}
			if (fade_count > 3)
				fade_count = 1;
			if (intensity == 1)
				Sleep(500);
			for (int j = 0; j <17; j++)
			{
				for (int index = 0; index < 30; index++)
				{
					if (fade_count == 1)
						dmxdata[i] = (rainbow2[index] / (intensity / 10));
					else if (fade_count == 2)
						dmxdata[i] = (rainbow3[index] / (intensity / 10));
					else if (fade_count == 3)
						dmxdata[i] = (rainbow1[index] / (intensity / 10));
					i += 1;
				}
			}
			if (step_up)
				intensity += 2;
			else
				intensity -= 2;
			SendDMXToPixe(dmxdata);
			counter++;
		} while (counter < 999);
	}
}

void ofxPixie::SendRGBChase()
{
	int counter = 0;
	uint8_t dmxdata[MAX_PIXIE_CHANNELS + 1];
	int i = 0;
	int rgb_scroll_start = 0;
	int ch_value = 0;
	int rgb_sequence = 0;

	if (device_handle != NULL)
	{
		// Looping to Send DMX data
		printf("\n Press Enter to Start RGB Chase (lasts 10 sec):");
		_getch();
		do {
			memset(dmxdata, 0, sizeof(dmxdata));
			ch_value = 255;
			rgb_scroll_start += 3;
			if (rgb_sequence > 2)
				rgb_sequence = 0;
			if (rgb_scroll_start > (MAX_PIXIE_CHANNELS - 30))
			{
				rgb_scroll_start = 0;
				rgb_sequence += 1;
			}
			i = rgb_scroll_start + rgb_sequence;
			while (i < (rgb_scroll_start + 30))
			{
				dmxdata[i] = ch_value;
				i += 3;
			}
			SendDMXToPixe(dmxdata);
			Sleep(10);
			counter++;
		} while (counter < 500);
	}
}


/* Function : SendDMXToPixe
* Author	: ENTTEC
* Purpose  : Sends the passed data array to Pixie; Define output config and universe as required (above)
* Parameters: dmx data array (510 bytes)
**/
void ofxPixie::SendDMXToPixe(uint8_t* dmxdata,  const std::vector<uint8_t> & PixieOutputs , const uint8_t & PixieOutputConfig)
{
	// Use this to target the correct section of each output (refer to API for more details)
	// Currently set to output 2 universes on both outputs
	//uint8_t PixieOutputs[] = { 0 }; //{ 0,1,2,3 };
	//uint8_t PixieOutputConfig = 1;

	uint8_t output_count = PixieOutputs.size(); //sizeof(PixieOutputs) / sizeof(PixieOutputs[0]);
	uint8_t output_universe = 0;
	for (uint8_t i = 0; i< output_count; i++)
	{
		output_universe = PixieOutputs[i];
		dmxdata[510] = 0 + (PixieOutputConfig << 2);
		if (output_universe > 0)
			dmxdata[510] += (output_universe << 0);
		//if (output_count == 1)
			//Sleep(4); //25
		// actuall send to Pixie now ..
		FTDI_SendData(LED_UPDATE_LABEL, dmxdata, MAX_PIXIE_CHANNELS + 1);
	}
}


void ofxPixie::connect()
{
	printf("\nEnttec PIXIE - C - Windows - Sample Test - VERSION %s \n", APP_VERSION);
	printf("\n Press Enter to Look for and connect to Pixie :");
	//_getch();

	// If you face problems identifying the USB DEVICE: Use this code to reload device drivers: takes a few secs
	// FTDI_Reload();

	// Create the device information list 
	ftStatus = FT_CreateDeviceInfoList(&Num_Devices);
	if (ftStatus == FT_OK)
		printf("\n Number of USB Devices found: %d\n", Num_Devices);
	if (Num_Devices > 0)
	{
		// got through all devices found to get info 
		for (i = 0; i<Num_Devices; i++)
		{
			device_num = i;
			printf("\n Testing Device: %d\n", device_num);
			// Returns usb-level info
			FT_GetDeviceInfoDetail(0, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &DevHandler);
			if (ftStatus == FT_OK)
			{
				printf("\n\t SerialNumber: %s\n", SerialNumber);
				printf("\n\t Description: %s\n", Description);
				FT_Close(DevHandler);
			}
			device_connected = isPixie(device_num);
			if (device_connected)
				break;
		}
	}
}


void ofxPixie::DMX_DATA::setLevel(unsigned int channel, uint8_t level) {
	if (channel >= sizeof(DMX)) return;
	if (level != DMX[channel]) {
		DMX[channel] = level;
		needsUpdate = true;
	}
}

void ofxPixie::DMX_DATA::clear() {
	memset(DMX, 0, sizeof(DMX));
}

unsigned char ofxPixie::DMX_DATA::getLevel(unsigned int channel) {

	return DMX[channel];
}

void ofxPixie::update(bool force)
{
	for (int universe = 0; universe < 4; universe++)
	{
		auto & dmx_d = dmx_data[universe];
		if ( force || dmx_d.needsUpdate) {
			SendDMXToPixe(dmx_d.DMX, dmx_d.PixieOutputs, dmx_d.PixieOutputConfig);
		}
	}
}


// our main function with everything to do the test
int ofxPixie::mainTest()
{
	if (device_connected)
	{
		// Send DMX to Pixie Examples
		auto & DMX = dmx_data[0].DMX;
		// Send All to white
		memset(DMX, 0, sizeof(DMX));
		for (int x = 0; x< MAX_PIXIE_CHANNELS; x++)
			DMX[x] = 255;
		SendDMXToPixe(DMX);
		Sleep(999);

		// Send All to green
		memset(DMX, 0, sizeof(DMX));
		SendDMXToPixe(DMX);
		for (int x = 0; x< MAX_PIXIE_CHANNELS; x++)
		{
			if (x % 3 == 0)
				DMX[x] = 199;
		}
		SendDMXToPixe(DMX);
		Sleep(999);

		// Send All to pink
		memset(DMX, 0, sizeof(DMX));
		SendDMXToPixe(DMX);
		for (int x = 0; x< MAX_PIXIE_CHANNELS; x++)
		{
			if (x % 3 == 0)
				DMX[x] = 0;
			else if (x % 2 == 0)
				DMX[x] = 99;
			else
				DMX[x] = 0;
		}
		SendDMXToPixe(DMX);

		// Send Dynamic Examples
		SendRGBChase();
		SendRainbowFades();

	}

	// Finish all done
	//printf("\n Press Enter to Exit :");
	//_getch();
	return 0;
}