#pragma once
#define __int8_t_defined
#define __uint32_t_defined

#include <stdint.h>
#include "usb_driver.h"
#include <vector>
//#define MAX_PIXIE_CHANNELS_4_UNIVERSE  (4 * (MAX_PIXIE_CHANNELS + 1))

class ofxPixie
{
public:
	class DMX_DATA
	{
		public:
			DMX_DATA() {
				//PixieOutputs.push_back(0);
				//PixieOutputs.push_back(1);
				//PixieOutputs.push_back(2);
				//PixieOutputs.push_back(3);
			}
			~DMX_DATA() {

			}


			void setLevel(unsigned int channel, unsigned char level);
			void clear();
			unsigned char getLevel(unsigned int channel);
			void setPixieOutputs(const std::vector<uint8_t> & pixieoutput)
			{
				PixieOutputs = pixieoutput; //{ 0,1,2,3 };
			}
			void setPixieOutputConfig(const uint8_t & pixieOutputConfig)
			{
				PixieOutputConfig = pixieOutputConfig; //{ 0,1,2,3 };
			}
			uint8_t DMX[MAX_PIXIE_CHANNELS + 1];
			std::vector<uint8_t> PixieOutputs; //{ 0,1,2,3 };
			uint8_t PixieOutputConfig = 1;
			bool needsUpdate = false;
	};

	ofxPixie(){
	};
	~ofxPixie() {};
	void connect();
	uint16_t getDeviceConnected() {
		return device_connected;
	}
	void update(bool force = false); // send a packet to the dmx controller

	DMX_DATA & GetDmxData(int universe)
	{
		return dmx_data[universe];
	}

protected:
	void SendRainbowFades();
	void SendRGBChase();
	void SendDMXToPixe(uint8_t* dmxdata)
	{
		SendDMXToPixe(dmxdata, dmx_data[0].PixieOutputs, dmx_data[0].PixieOutputConfig);
	}
	void SendDMXToPixe(uint8_t* dmxdata, const std::vector<uint8_t> & PixieOutputs, const uint8_t & PixieOutputConfig);
	int mainTest();
	uint16_t isPixie(int device_num);
private:
	void FTDI_ClosePort();
	void FTDI_Reload();
	int FTDI_SendData(int label, uint8_t *data, int length);
	int FTDI_ReceiveData(int label, uint8_t *data, unsigned int expected_length);
	void FTDI_PurgeBuffer();
	


	PixieConfigGet PixieParams;
	FT_HANDLE device_handle = NULL;
	DWORD Num_Devices = 0;
	uint16_t device_connected = 0;
	int i = 0;
	int device_num = 0;
	BOOL res = 0;
	FT_STATUS ftStatus;
	FT_HANDLE DevHandler;
	DWORD Flags;
	DWORD ID;
	DWORD Type;
	DWORD LocId;
	char SerialNumber[16];
	char Description[64];
	uint8_t output_config = 2;
	uint8_t output_universe = 0;



	DMX_DATA dmx_data[4];

};